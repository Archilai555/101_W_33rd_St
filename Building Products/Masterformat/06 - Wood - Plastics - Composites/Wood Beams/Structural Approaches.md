- Primary Structural Members at Balconies 
	- Wood
		- General
		     - Pros
			     - Less expensive than steel
			     - Warm feeling
		     - Cons
			     -  requires more maintenance--applying an exterior stain/paint every 5 years or so
			     - needs to be pressure treated
				     - requires dark stain or paint to offset 'green' color
			     -  exposed top edge (under composite decking) needs metal flashing like [this](https://www.swedishwood.com/optimized/default/siteassets/1-trafakta/4-om-limtra/en/examplestructuresen.png).  Only for major beams, not joists.
			     - annual termite control
	  - Specific Types
		  - Glulams (pressure treated)
			  - visual examples, [here](https://www.google.com/search?q=glulam%20beams&tbm=isch&ved=2ahUKEwjVxfG4mN31AhXRB50JHQZFBTIQ2-cCegQIABAA&oq=glulam%20beams&gs_lcp=CgNpbWcQAzIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQ6CAgAEIAEELEDUKsJWJgUYLQWaANwAHgAgAF8iAGdBpIBAzAuN5gBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=Fnf4YdXiA9GP9PwPhoqVkAM&bih=937&biw=1920&rlz=1C1CHBF_enUS963US963).
		     - Pros
			     - a nice wood grain look, takes stain well
		     - Cons
			     - probably as expensive as steel

		   - LVL or PSL (pressure treated)
			   - visual examples
				   - [LVLs](https://www.google.com/search?q=LVL%20BEAMS&tbm=isch&ved=2ahUKEwim-9LRmN31AhWBRc0KHeq9BHsQ2-cCegQIABAA&oq=LVL%20BEAMS&gs_lcp=CgNpbWcQAzIECAAQQzIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBQgAEIAEMgUIABCABDIFCAAQgAQyBggAEAcQHjIGCAAQBxAeUO1SWO1SYM5daABwAHgAgAHWAYgBwQOSAQUwLjIuMZgBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=SXf4YeayPIGLtQbq-5LYBw&bih=937&biw=1920&rlz=1C1CHBF_enUS963US963)
				   - [PSLs](https://www.google.com/search?q=psl%20BEAMS&tbm=isch&ved=2ahUKEwiW3dHZmN31AhVQBc0KHR5zDScQ2-cCegQIABAA&oq=psl%20BEAMS&gs_lcp=CgNpbWcQAzIFCAAQgAQyBggAEAUQHjIECAAQGDIECAAQGDIECAAQGDoECAAQQzoGCAAQBxAeUOIHWNAPYPYQaABwAHgAgAGBAYgB3QSSAQMwLjWYAQCgAQGqAQtnd3Mtd2l6LWltZ8ABAQ&sclient=img&ei=Wnf4YZbILdCKtAae5rW4Ag&bih=937&biw=1920&rlz=1C1CHBF_enUS963US963)
			   - Pros
				   - Less expensive than glulams
			   - Cons
				   - Finish is not as nice as a glulam
				   - Wood stamps need to be sanded off or dark stain or paint to cover up. 
	- Steel
		- Hot dipped Galv. steel
			- Cons
				- More expensive than wood
					- when weighing the cost of staining, metal flashing, and periodic maintenance of wood, the cost difference might be more negligible 
			- Pros
				- doesn't need an additional finish other than the HDG
				- no flashing at top edge
				- No termite management

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE1Mjg5NzQxMSwtMjA5OTg1NTQzMyw1Mj
IwMDkyNjIsNTk4MzQ2NTg2XX0=
-->