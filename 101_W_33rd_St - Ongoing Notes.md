- Overall context
  - They would like to develop the [entire lot](https://www.dropbox.com/s/p3yp1bos8cq3edn/2021-10-21_16-23-57_Autodesk_Revit_2020.2_-_%5B101_W_33rd_St.rvt_-_Floor_Revit.png?dl=0), if possible. 
  - They might purchase [these](https://www.dropbox.com/s/lunl9ond6esa79l/2021-10-21_16-26-13_Autodesk_Revit_2020.2_-_%5B101_W_33rd_St.rvt_-_Floor_Revit.png?dl=0) (2) relatively soon, 
  - but [these](https://www.dropbox.com/s/ecsv6s84mpmu9dt/2021-10-21_16-26-46_Autodesk_Revit_2020.2_-_%5B101_W_33rd_St.rvt_-_Floor_Revit.png?dl=0) 2 lots are unlikely to purchased in the near future. 
    - [This](https://www.dropbox.com/s/vzzsk4iv2prm7kw/2021-10-21_16-28-36_Autodesk_Revit_2020.2_-_%5B101_W_33rd_St.rvt_-_Floor_Revit.png?dl=0) area of Finfeather Rd will and is abandoned by the city. 
  - Clients are interested in purchasing [these](https://www.dropbox.com/s/55270eniluuas0y/2021-10-21_16-30-17_Autodesk_Revit_2020.2_-_%5B101_W_33rd_St.rvt_-_Floor_Revit.png?dl=0) two lots, but will most likely not happen in the near future. 
  - The city has expressed interest in allowing [this](https://www.dropbox.com/s/2yps0kwfs1yn35r/2021-10-21_16-31-47_Autodesk_Revit_2020.2_-_%5B101_W_33rd_St.rvt_-_Floor_Revit.png?dl=0) street to be privatized.
    - Potential parking area for future development.
- Overall building guidlines.
  - Sketch of concept [here](https://openingdesign.first-draft.xyz/d/20211021_101_W_33rd_St/?l=-508&t=-166&r=2101&b=987&z=0.7). 
  - to reduce parking count, make 1 bedrooms with study/den area.
  - no amenity spaces needed
    - however potential community 'courtyard area' if/when there's a larger development of the lot. 
    - Like idea of exterior stair.
  - Possible to explore other site layouts but without reducing parking. 
  - Look at potentially laying out entire block with future development to see if that informs a certain layout. 
  - building potentially might not need an elevator--require further research.
  - Explore prefabricated panelized approaches
    - share precedents

- Units
  - Studios: 650-750sf
  - 2bd: 800-900sf

### 2021/11/05 - Meeting with owners

- Orientation of balconies?
  - toward street
- Balconies off bedroom?
  - no
- No common area?  mail?
  - yes mail
- Commercial?
  - no, not on this lot
- 2bd
  - 2nd bath?
    - no
- Any preferences on materials?
  - no
- Parking?
  - pervious?
    - no
- costs precedents
  - $125/sq projected
- Have a chat with construction superviser?
  - structural systems?
    - Yes, set up time with them, and possible prefabricator
- [this](https://www.dropbox.com/s/rv6a0i4xkxwlsps/2021-11-05_10-23-49_Green_House_SUrvey_-_Bluebeam_Revu_x64_Revu.png?dl=0) area is one lot
- Try to get more units.
  - shooting for 26 beds.
- Commercial on 1st floor for 2nd phase, located [here](https://www.dropbox.com/s/2jaqmxj6ecfbs1i/2021-11-05_08-12-45_Zoom_Meeting_Zoom.png?dl=0f)

### 20211119 - Meeting w/ Owners and Zoning Department

- From owner
  - Survey?
    - to locate water, sanitary sewer, cable, television, gas, electric and telephone, topo?
      - Yes one is being commissioned
  - Civil is doing the landscaping plan?
    - yes
  - At L-shaped building: Would like all commercial on 1st floor 
  - Civil engineer: Glenn Jones -gjones@j4engineering.com
- to Zoning
  - Site
    - setbacks correct?
      - (2) front setback on Site B?
        - no
    - parking public utility easement?
      - yes
    - survey of adjacent property? or just a google maps guess?
      - not necessary
    - parking up to lot line?
      - yes, but not next to right-of-way
    - Can utilities, like transformer, compressors, dumpsters, go up to the lot line?  enclosure requirements?
      - yes can go up to lot line, unless it's a building that needs to be permitted.
    - dumpsters enclosure requirements?
      - no
        - 6ft fence--see zoning requirements
        - Owners don't want gates around trash
    - detention ponds?
      - no
    - Photometric study on light levels study
      - no

---

- Future Questions
  - -is there a GIS sources that shows elevation contours?
  - Bike parking requirements?

### 20220110 - Meeting w/ Owners

- Survey still inbound, likely Thursday of this week. 
- Owners meeting with MEP and will look into pricing considerations of each option.
- Owners very positive about Option one.
  - Likes the wood look and material changes
  - Don't like the maintenance of wood - open to exploring similar looking alternatives if not too expensive
  - Likes expanded metal look if not too expensive
  - Sara likes brick look on first floor, possibly open to board formed concrete
  - Like the ''pitched' roof look used to hide HVAC
- Owners strong negative reaction to 2nd option. 
- No islands in kitchen - if absolutely necessary, only 6' long so can purchase pre-made with butcher block top
- Side-by-side washer/dryer in units if room. more cost effective. 
- Smaller closet spaces
- No Pocket Doors
- Don't make bathroom wider than tub
- Vanity 42" or 48" max
- No pocket doors unless necessary
- Tubs everywhere
- Kitchens need a pantry
- Prioritize front entry wow factor and views through units
- Elec panel box out of sight
- Owners want acoustic wall construction between units: staggered studs w/1" air gap
- no exhaust fans in rated walls between units to avoid fire dampers
- 30" fridge
- Explore feature walls for end wall opposite front entry and short wall separating bed space from living space
- Some unit plans will have feature wall separating bed space from living space. Others will be completely open. Owner to determine percentage of each.
- Explore options for feature wall not going full height to ceiling.
- 9' ceiling miniumum preferred; top floor explore open structure look
- Generally speaking, Owner likes straight walls inside and varied dimensions outside
- need to coordinate chase locations, HVAC recommendations w/ MEP consultant
- no tempered windows if possible - too expensive
- Owner has sent markups of unit layouts to incorporate.


### 20220211 - Meeting w/ Owners

- Approach at pricing steel, glulams?
- Staining/Painting of Glulams/lumber
	- pressure treated slightly green
		- okay with seeing stamps.
		- okay with exploring various stain/paint opacities.
		- expose glulam inside unit.
- Windows?
	- Double hung okay?
		- yes
		- Andersen double hung 400?
			- depends on pricing
		- cottage style
	- Fixed picture near kitchen
		- yes, likes
- Siding
	- 1st
		- james hardie
	- 2nd & 4th band?
		- corrugated metal
	- 3rd band
		- batten board
- Not so hot on the copper color
	- OD to suggest another color
- Railing
	- (2) treated 2x4's sandwich horse fence?
		- okay
- Privacy walls
	- 4x4's?
		- yes
	- 1x trim
- Gutters?
	- no
- half high concrete walls at first floor?
	- yes, might get priced out later
- Stairs
	- (2) stringers
	- galv. grate or stock steel tread




---


- Units
	- swing out doors?
- patios on south side?
	- doors on 1st floor at east/west units?




- Meeting with MEP
	- HVAC
		- Duct routing?
			- trusses
			- 	Main chase for coolant lines?
	- Electrical
		- Location of panels
	- Plumbing
		- Water heater?
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTExMDczMzA2ODAsLTY4NjQ5MTcxOCwtOD
k2Mzk1OTcwLDIwMTIwMDQ1ODMsMTkzNDMzNDcwNCwxNjc2NjQ1
MjYzLC0xMzk5NjM4MDY0LDIwMjM0Mjg5MiwxMTc3NDE0NDMwLC
0xMjQ1MDEwMDgsLTE3MzE3NzY5NjMsLTgxNTM5NzY5LC00NDgz
OTA4OTEsNjIwMDU2MzQ2LC01NDY4Nzk2MzcsLTIxMzA0NTE4LC
0xNTQ2NDM5MTI4LC0xMDU1NzE5MzkwLDEyOTE3MjcwODEsLTEz
NjU5Mjg2MTRdfQ==
-->