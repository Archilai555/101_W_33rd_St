- Zoning
  - Randy Haynes AICP
  - Planning Administrator 
  - rhaynes@bryantx.gov 
  - (979) 209-5030

- Plan Review
  - Karen Lahde
  - Plans Examiner-Commercial
  - krudasill@bryantx.gov
  - (979) 209-5030

- [Brazos County GIS](https://www.bryantx.gov/information-technology/gis-mapping-services/gis-maps/)
  
  - [GIS of site](https://gis.bisclient.com/brazoscad/index.html?find=354390)
  - [Google map](https://www.google.com/maps/place/101+W+33rd+St,+Bryan,+TX+77803/@30.6667783,-96.3746131,170m/data=!3m1!1e3!4m5!3m4!1s0x86468199b96a8d93:0xe4de3001305083b7!8m2!3d30.6669793!4d-96.3741984)

- ZONING    
  
  - [List of ordinances numbers](https://library.municode.com/tx/bryan/ordinances/code_of_ordinances?nodeId=2021)
  - [Chapter 130 - ZONING](https://library.municode.com/TX/bryan/codes/code_of_ordinances?nodeId=PTIICOOR_CH130ZO)
    - [Sec. 130-37. - Screening fence standards.](https://library.municode.com/tx/bryan/codes/code_of_ordinances?nodeId=PTIICOOR_CH130ZO_ARTIIIOTRE_S130-37SCFEST)
  - [Chapter 62 - LAND AND SITE DEVELOPMENT](https://library.municode.com/tx/bryan/codes/code_of_ordinances?nodeId=PTIICOOR_CH62LASIDE)
    - [ORDINANCE NO.  2501](https://library.municode.com/tx/bryan/ordinances/code_of_ordinances?nodeId=1101895) supersedes things in Chapter 62 above
      - (MT-C) MIDTOWN CORRIDOR DISTRICT (MT-C)
    - [ORDINANCE NO. 2074]([Municode Library](https://library.municode.com/tx/bryan/ordinances/code_of_ordinances?nodeId=702181)) - Design Guidelines
    - [Sec. 62-297. - Parking and circulation](https://library.municode.com/tx/bryan/codes/code_of_ordinances?nodeId=PTIICOOR_CH62LASIDE_ARTVIACOREPA_DIV2ACPACIPR_S62-297PACI)
      - (i) Amount of parking required.
  - [Midtown](https://www.bryantx.gov/midtown/)
    - [Midtown area plan](https://docs.bryantx.gov/projects/midtown-map/Midtown-Plan.pdf)

- Sec 62-593 - Lot development standards
  
  - Setbacks
    - Front (all other streets) - 10' minimum, 30’ maximum
    - Side (Adjacent lot) (all other streets) – 5’ minimum
    - Rear (adjacent lot) – 5’ minimum
    - *Setback shall mean the distance between the outside wall of the main building and any lot line. The setback may exclude uncovered walks, chimneys, bay windows, and roof _overhangs_ up to 18 inches in width.*
  - Building Frontage Required
    - Minimum of 30% of the building facades on properties along all other streets should be built to the setback range
  - Maximum building height: 
    - 3 stories (35 feet) 
  - Maximum buildable area: 
    - 90% of the lot area may be impervious cover.
  - Landscaping requirements
    - Notwithstanding the requirements of Sec. 62-429(1)(a), an area equal to 10% of the developed area (building site) shall be required to be landscaped.
    - Landscaping shall be located along the front and sides of the properties in those portions of the developed area facing a public right-of-way. 
    - Where street improvements are being made by a developer as part of a development project, irrigated landscaping and street trees installed in the right-of-way, may be permitted in lieu of landscaping requirements. This administrative allowance is determined by the SDRC Chair or his/her designee.

- Sec 62-594 – Access and Parking Standards.
  
  - Off-street parking should only be located behind or to the side of buildings, as seen from the public right-of-way. 
  - On-street parking counted. At the discretion of the City Engineer, new or existing on-street parking, where allowed in the public right-of-way and built to the following standards, may be counted towards the total off-street parking requirement for a project.
    - On-street parking shall be prohibited in the following places as described by the Texas Transportation Code
      - i. In front of a public or private driveway;
      - ii. Within 15 feet of a fire hydrant;
      - iii. Within 20 feet of a crosswalk at an intersection;
      - iv. Within 30 feet on the approach to a flashing signal, stop sign, yield sign, or traffic-control signal located at the side of a roadway;
      - v. Within 50 feet of a railroad crossing;
      - vi. Where an official sign prohibits parking.
  - On-street parking area design shall comply with the minimum standards set forth in the [Parking Area Design section](https://library.municode.com/tx/bryan/codes/code_of_ordinances?nodeId=PTIICOOR_CH62LASIDE_ARTVIACOREPA_DIV2ACPACIPR_S62-297PACI)
  - Only on-street parking spaces located entirely in front of a subject property may be claimed towards the total off-street parking requirements for a project.
  - Off-street parking and minimum parking requirements
    - (5) Lodging.
      - i. Midtown – Corridor District (MT-C) - 1.0 spaces per lodging room

- From Midtown Area Plan
  
  - Driveway width. Maximum driveway width is 12’, except when the driveway serves more than 4 units or commercial uses. For more than 4 units, the maximum width is 20’. Maximum curb cut radius is 10’ on streets which aren’t arterials.
  - ideally parking inside the lot, with building facing the street. 

GIS: https://gis.bisclient.com/brazoscad/index.html?find=354390
    - Property ID: 354390
    - Legal Description: CITY OF BRYAN TOWNSITE, BLOCK 96, LOT 3-5 & PT OF 6 & PT OF ALLEY
    - Tract or Lot: 3-5 & PT OF 6 & PT OF ALLEY
    - Abstract Subdivision Code: 191000
    - Block: 96
    - Neighborhood Code: 1531250H
    - School District: S1
    - City Limits: C1

---

- [Architectural Barriers Texas Accessibility Standards (TAS)](https://www.tdlr.texas.gov/ab/abtas.htm)
  - [208 Parking Spaces](https://www.tdlr.texas.gov/ab/2012abtas2.htm#208)
  - [502 Parking Spaces](https://www.tdlr.texas.gov/ab/2012abtas5.htm#502)

---

- [2010 ADA Standards](https://up.codes/viewer/ada)
  - 233 Residential Facilities
    - [233.3.1.1 Residential Dwelling Units With Mobility Features](https://up.codes/viewer/ada/chapter/2/scoping-requirements#233.3.1.1)

---

- [International Building Code 2015 (IBC 2015) for Texas](https://up.codes/viewer/texas/ibc-2015)
  - [1107.6.2.2.1 Type A Units](https://up.codes/viewer/wyoming/ibc-2015/chapter/11/accessibility#1107.6.2.2.1)
  - [1107.6.2.2.2 Type B Units](https://up.codes/viewer/wyoming/ibc-2015/chapter/11/accessibility#1107.6.2.2.2)
    - Exception: The number of Type B units is permitted to be reduced in accordance with Section 1107.7
      -
      
      <!--stackedit_data:
      eyJoaXN0b3J5IjpbNTE1NDU3NTMsLTE3NTIyNTQzMywtMTU4OD
      c0NTQ0NSw5NzM5Nzc0MTksNDAwNzQyMjQxLDc0Mzg0NzkyMSwx
      ODA2ODMxMzU0LC02MjAzNTk1MTcsLTYxNDIxMzM2OCwyMTc5Mj
      AxODcsLTEwNTk2NTk5OTAsLTY5MjI1ODY5NSw1MzYwNTU5Njgs
      LTEyMjAwOTIxNjMsMTIwMDEwMDg2LDg3Njc3NDE3Miw1NjQ2NT
      AxNjUsMTQwNzk0Njg4MSwtMTEyNzg0OTY0MiwyNDg1MzI4NTdd
      fQ==
      -->